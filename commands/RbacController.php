<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        // add "changeStatus" permission
        $changeStatus = $auth->createPermission('changeStatus');
        $changeStatus->description = 'Change Status of a Task';
        $auth->add($changeStatus);

        // add "assignUserToProject" permission
        $assignUserToProject = $auth->createPermission('assignUserToProject');
        $assignUserToProject->description = 'Assign User To Project';
        $auth->add($assignUserToProject);
		
		// add "editTask" permission
        $editTask = $auth->createPermission('editTask');
        $editTask->description = 'Edit Task';
        $auth->add($editTask);
		
		// add "createTask" permission
        $createTask = $auth->createPermission('createTask');
        $createTask->description = 'Create Task';
        $auth->add($createTask);
		
		// add "assignUserToTask" permission
        $assignUserToTask = $auth->createPermission('assignUserToTask');
        $assignUserToTask->description = 'Assign User To Task';
        $auth->add($assignUserToTask);
		
		// add "deleteTask" permission
        $deleteTask = $auth->createPermission('deleteTask');
        $deleteTask->description = 'Delete Task';
        $auth->add($deleteTask);

        // add "perform_task" role and give this role the "changeStatus" permission
        $perform_task = $auth->createRole('perform_task');
        $auth->add($perform_task);
        $auth->addChild($perform_task, $changeStatus);

        // add "project_manager" role and give this role the "assignUserToProject", "editTask", 
		// "createTask", "assignUserToTask", "deleteTask" permissions
        // as well as the permissions of the "perform_task" role
        $project_manager = $auth->createRole('project_manager');
        $auth->add($project_manager);
        $auth->addChild($project_manager, $assignUserToProject);
		$auth->addChild($project_manager, $editTask);
		$auth->addChild($project_manager, $createTask);
		$auth->addChild($project_manager, $assignUserToTask);
		$auth->addChild($project_manager, $deleteTask);
        $auth->addChild($project_manager, $perform_task);
		
		// add "ceo" role and give this role permission of "project_manager"
        $ceo = $auth->createRole('ceo');
        $auth->add($ceo);
        $auth->addChild($ceo, $project_manager);
		
		// add "admin" role and give this role permission of "admin"
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $ceo);

        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        //$auth->assign($author, 2);
        //$auth->assign($admin, 1);
    }
}