<?php

use yii\db\Migration;

class m170715_101041_create_project extends Migration
{
    public function up()
    {
		$this->createTable('project', [
            'id' => $this->primaryKey(),
			'name' => $this->string()
		]);
    }

    public function down()
    {
        $this->dropTable('project');
    }

}
